 # Curso de Base de Datos


## Programa del Curso
* [Programa del Curso de Base de Datos](/resources/pdf/programa_curso_base_de_datos.pdf "Programa del Curso")
* [Conograma del Curso](/resources/planificacion_sem_2_2023.ods "Cronograma del Curso")

## Proyecto
* Pauta de Evaluación [ [ODP](/resources/proyecto/Proyecto.odt | [PDF](/resources/proyecto/Proyecto.pdf ) ] 
* Avance  - `9 de Octubre`  [ [ODP](/resources/proyecto/templata_entrega_avance.odt) | [PDF](/resources/proyecto/templata_entrega_avance.pdf) ]
 
* Presentación Final - `Desde el 4 de Diciembre` [ [ODP](/resources/proyecto/Presentacion.odt) | [PDF](/resources/proyecto/Presentacion.pdf) ] 


## Clases
* [Material de las clases](https://gitlab.com/l30bravo/db_udp/-/tree/main/resources/clases)


## Evaluaciones Pasadas
* [Material de las clases](https://gitlab.com/l30bravo/db_udp/-/tree/main/resources/evaluaciones)

## Apuntes
* [¿Como instalar Postgres?](postgres_install.md)
* [Actualizar password de Postgres](postgres_update_pass.md)
* [¿Como instalar PGModeler?](https://gitlab.com/l30bravo/db_udp/-/blob/main/resources/scripts/pg_modeler_install.sh)


## Tools
* [Draw.io](https://github.com/jgraph/drawio-desktop) - Software para modelar
* [DBeaver](https://dbeaver.io/) - UI para consultar DBs (PostgreSQL, MySQL, SQLite, Oracle, etc)
* [PGModeler](https://github.com/pgmodeler/pgmodeler) - UI para modelar y consultar DBs Postgre (crea el modelo de datos e implemta el modelo en una DB Postgre)
* [PGModeler - Instalador para Archlinux](/resources/scripts/pg_modeler/pg_modeler_install_archlinux.sh) - Instalador de PGModeler para Archlinux)
* [PGModeler - Instalador para Debian / Ubuntu o deribados](/resources/scripts/pg_modeler/pg_modeler_install_ubuntu.sh) - Instalador de PGModeler para Debian / Ubuntu y deribados)
## Bibliografia
* [Introducción a los sistemas de Base de Datos](/resources/pdf/Introduccion%20a%20los%20Sistemas%20de%20Bases%20de%20Datos%20-%207ma%20Edicion%20-%20C.%20J.%20Date.pdf "Introducción a los sistemas de Base de Datos")

* [Manual de PostgreSQL](/resources/pdf/Postgres-User.pdf "Manual de PostgreSQL")
